import 'package:flutter/material.dart';

import 'package:projek/pages/login.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/',
    routes: {
      '/' : (context) => Login(),
    },
  ));
}