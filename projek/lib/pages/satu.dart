import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:projek/pages/donasi.dart';

class Satu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[300],
          title: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color : Colors.white,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (context) => Donasi()),
                          );
                        },
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("Donasi", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold)),
                        ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        body : SatuPage(),
      ),
    );
  }
}

class SatuPage extends StatefulWidget {
  @override
  _SatuPage createState() => _SatuPage();
}

class _SatuPage extends State<SatuPage> {

  _launchURL() async {
  const url = 'https://donasi.online/panti-yatim-indonesia';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

  Widget titleSection = Container(
      padding: EdgeInsets.only(top: 16),
      child: Text(
        'Panti Asuhan Indonesia',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 16,
        ),
      ),
    ); 

    Widget subtitleSection = Container(
      padding: EdgeInsets.only(top: 16),
      child: Text(
        '05 - 21 Mei 2020',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 14,
          color: Colors.red,
        ),
      ),
    );

    Widget descriptionSection = Container(
      padding: EdgeInsets.all(16),
      child: Text(
        'Panti Yatim Indonesia adalah Lembaga Amil Zakat Nasional (LAZNAS) ' 
        'berdasarkan SK Kemenag RI No.120 Tahun 2019. Bertekad menjadi lembaga '
        'pengelola dana ZISWAF yang berkhidmat menangani anak yatim / piatu dan '
        'terlantar serta memberdayakan kaum dhuafa.',
        textAlign: TextAlign.justify,
        style: TextStyle(
          fontSize: 14,
        ),
      )
    );


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
          children: <Widget>[
            Image.asset('assets/donasi2.png'),
            Container(
              padding: EdgeInsets.only(bottom: 24),
            ),
            titleSection,
            subtitleSection,
            descriptionSection,
            Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            child: FlatButton(
                              onPressed: _launchURL,
                              color: Colors.lightBlue[300],
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: Text("Halaman", style: TextStyle(color: Colors.white, fontSize: 13.0)),
                            ),
                          ),
                        ],
                      ),
                    ),
          ]
      )
     );
  }
}