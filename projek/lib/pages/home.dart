import 'package:flutter/material.dart';

import 'package:projek/pages/notifikasi.dart';
import 'package:projek/pages/spp_kuliah.dart';
import 'package:projek/pages/praktikum.dart';
import 'package:projek/pages/donasi.dart';
import 'package:projek/pages/tertunda_kosong.dart';
import 'package:projek/pages/riwayat_transaksi.dart';
import 'package:projek/pages/pengaturan.dart';
import 'package:projek/pages/rekening.dart';
import 'package:projek/pages/tambah_tabungan.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body: ListView(
        children: <Widget>[
          _top(),
          SizedBox(height: 30.0),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Menu",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                ),
                Text(
                  "Semua Menu",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0),
                )
              ],
            ),
          ),
          SizedBox(height: 20.0),
          Container(
            height: 180.0,
            child: GridView(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3, childAspectRatio: 3/2),
              children: <Widget>[
                _gridItem(),
                _gridItem2(),
                _gridItem3(),
                _gridItem4(),
                _gridItem5(),
                _gridItem6(),
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.all(16.0),
            child: Row(
              children: <Widget>[
                Text(
                  "Rekening Tabungan",
                  style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
          ),
          _cardItem1(1),

          Padding(
            padding: EdgeInsets.all(16.0),
            child: Row(
              children: <Widget>[
                Text(
                  "Tambah Rekening Tabungan",
                  style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
          ),
          _cardItem2(1),

        ],
      ),
    );
  }

  _cardItem1(image){
    return Padding(
      padding: EdgeInsets.all(16.0),
      child : Row(
        children: <Widget>[
          Container(
            width: 100.0,
            height: 100.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/mandiri.png'),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(10.0)
            ),
          ),
          SizedBox(width: 20.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Mega Oktaviani Fadillah",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0,
                ),
              ),
              SizedBox(height: 11.0),
              Text(
                "1881500095709",
                style: TextStyle(
                  color: Colors.lightBlue[300],
                  fontWeight: FontWeight.bold,
                  fontSize: 13.0,
                ),
              ),
               Container(
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10.0),
            child: FlatButton(
              onPressed: () {
                Navigator.push(
                  context, 
                  MaterialPageRoute(builder: (context) => Rekening()),
                );
              },
              color: Colors.lightBlue[300],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Text("Rincian", style: TextStyle(color: Colors.white, fontSize: 13.0,)),
            ),
          ),
            ],
          ),
        ],
      ),
    );
  }

   _cardItem2(image){
    return Padding(
      padding: EdgeInsets.all(16.0),
      child : Row(
        children: <Widget>[
          Container(
            width: 100.0,
            height: 100.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/latar.jpg'),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(10.0)
            ),
          ),
          SizedBox(width: 20.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Nama Pemilik",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0,
                ),
              ),
              SizedBox(height: 11.0),
              Text(
                "Nomor Rekening",
                style: TextStyle(
                  color: Colors.lightBlue[300],
                  fontWeight: FontWeight.bold,
                  fontSize: 13.0,
                ),
              ),
               Container(
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10.0),
            child: FlatButton(
              onPressed: () {
                Navigator.push(
                  context, 
                  MaterialPageRoute(builder: (context) => TambahTabungan()),
                );
              },
              color: Colors.lightBlue[300],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Text("Tambah", style: TextStyle(color: Colors.white, fontSize: 13.0,)),
            ),
          ),
            ],
          ),
        ],
      ),
    );
  }

  _gridItem(){
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50.0,
            width: 50.0,
            child: FloatingActionButton(
              heroTag: 'btn1',
              backgroundColor: Colors.lightBlue[300].withOpacity(0.9),
              child: Icon(
                  Icons.payment,
                  size: 25.0,
                  color: Colors.white,
              ),
              onPressed: (){
                Navigator.push(
                  context, 
                  MaterialPageRoute(builder: (context) => SPPKuliah()),
                );
              },
            ),
          ),
          SizedBox(height: 10.0),
          Text("SPP Kuliah", style: TextStyle(fontSize: 11.0)),
        ],
      ),
    );
  }

  _gridItem2(){
   return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50.0,
            width: 50.0,
            child: FloatingActionButton(
              heroTag: 'btn2',
              backgroundColor: Colors.lightBlue[300].withOpacity(0.9),
              child: Icon(
                  Icons.developer_board,
                  size: 25.0,
                  color: Colors.white,
              ),
              onPressed: (){
                Navigator.push(
                  context, 
                  MaterialPageRoute(builder: (context) => Praktikum()),
                );
              },
            ),
          ),
          SizedBox(height: 10.0),
          Text("Praktikum", style: TextStyle(fontSize: 11.0)),
        ],
      ),
    );
  }

  _gridItem3(){
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50.0,
            width: 50.0,
            child: FloatingActionButton(
              heroTag: 'btn3',
              backgroundColor: Colors.lightBlue[300].withOpacity(0.9),
              child: Icon(
                  Icons.account_balance_wallet,
                  size: 25.0,
                  color: Colors.white,
              ),
              onPressed: (){
                Navigator.push(
                  context, 
                  MaterialPageRoute(builder: (context) => Donasi()),
                );
              },
            ),
          ),
          SizedBox(height: 10.0),
          Text("Donasi", style: TextStyle(fontSize: 11.0)),
        ],
      ),
    );
  }

  _gridItem4(){
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50.0,
            width: 50.0,
            child: FloatingActionButton(
              heroTag: 'btn4',
              backgroundColor: Colors.lightBlue[300].withOpacity(0.9),
              child: Icon(
                  Icons.history,
                  size: 25.0,
                  color: Colors.white,
              ),
              onPressed: (){
                Navigator.push(
                  context, 
                  MaterialPageRoute(builder: (context) => RiwayatTransaksi()),
                );
              },
            ),
          ),
          SizedBox(height: 10.0),
          Text("Riwayat Transaksi", style: TextStyle(fontSize: 11.0)),
        ],
      ),
    );
  }

  _gridItem5(){
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50.0,
            width: 50.0,
            child: FloatingActionButton(
              heroTag: 'btn5',
              backgroundColor: Colors.lightBlue[300].withOpacity(0.9),
              child: Icon(
                  Icons.snooze,
                  size: 25.0,
                  color: Colors.white,
              ),
              onPressed: (){
                Navigator.push(
                  context, 
                  MaterialPageRoute(builder: (context) => TertundaKosong()),
                );
              },
            ),
          ),
          SizedBox(height: 10.0),
          Text("Transaksi Tertunda", style: TextStyle(fontSize: 11.0)),
        ],
      ),
    );
  }

  _gridItem6(){
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50.0,
            width: 50.0,
            child: FloatingActionButton(
              heroTag: 'btn6',
              backgroundColor: Colors.lightBlue[300].withOpacity(0.9),
              child: Icon(
                  Icons.settings,
                  size: 25.0,
                  color: Colors.white,
              ),
              onPressed: (){
                Navigator.push(
                  context, 
                  MaterialPageRoute(builder: (context) => Pengaturan()),
                );
              },
            ),
          ),
          SizedBox(height: 10.0),
          Text("Pengaturan", style: TextStyle(fontSize: 11.0)),
        ],
      ),
    );
  }

  _top(){
  return Container(
    padding: EdgeInsets.all(16.0),
    decoration: BoxDecoration(
      color: Colors.lightBlue[300],
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(30.0),
        bottomRight: Radius.circular(30.0),
      )
    ),
    child: Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: AssetImage('assets/avatar.png'),
                ),
                SizedBox(width: 10.0),
                Text("Hai, Mega Oktaviani Fadillah", style: TextStyle(color: Colors.white),),
              ],
            ),
            IconButton(
              icon: Icon(
                Icons.notifications,
                color : Colors.white,
              ),
                onPressed: (){
                  Navigator.push(
                    context, 
                    MaterialPageRoute(builder: (context) => Notifikasi()),
                  );
                },
            )
          ],
        ),
        SizedBox(height: 30.0),
        TextField(
          decoration: InputDecoration(
            hintText: "Mencari",
            fillColor: Colors.white,
            filled: true,
            suffixIcon: Icon(Icons.filter_list),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20.0),
              borderSide: BorderSide(color: Colors.transparent),
            ),
            contentPadding: 
              EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
          ),
        )
      ],
    )
  );
}
}