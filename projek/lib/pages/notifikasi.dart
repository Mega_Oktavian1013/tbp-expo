import 'package:flutter/material.dart';

import 'package:projek/pages/home.dart';

class Notifikasi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[300],
          title: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color : Colors.white,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (context) => MyApp()),
                          );
                        },
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("Notifikasi", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold)),
                        ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        body : NotifikasiPage(),
      ),
    );
  }
}

class NotifikasiPage extends StatefulWidget {
  @override
  _Notifikasi createState() => _Notifikasi();
}

class _Notifikasi extends State<NotifikasiPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
            child: Text(
              "Hari ini",
              style: TextStyle(color: Colors.lightBlue[500],
              fontSize: 16.0,
              fontWeight: FontWeight.bold)),
          ),
          _donasinull(),
          _donasipertama(),
          _donasikedua(),
          _donasiketiga(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
            child: Text(
              "Kemarin",
              style: TextStyle(color: Colors.lightBlue[500],
              fontSize: 16.0,
              fontWeight: FontWeight.bold)),
          ),
          _donasikeempat(),
          _donasikelima(),
        ],
      ),
    );
  }
  _donasinull(){
  return Column(
    children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40.0,
                              height: 40.0,
                              child: CircleAvatar(
                                child: Icon(
                                  Icons.payment,
                                  size: 25.0,
                                  color: Colors.white,
                                ),
                                backgroundColor: Colors.blue[700].withOpacity(0.9),
                              ),
                            ),
                            SizedBox(width: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Pembayaran Praktikum Berhasil", style: TextStyle(color: Colors.blue[700], fontSize: 14.0, fontWeight: FontWeight.bold)),
                            SizedBox(height: 10.0,),
                            Text("05 Mei 2020   13.20", style: TextStyle(color: Colors.blue[700], fontSize: 10.0)),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
  );
}

  _donasipertama(){
  return Column(
    children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40.0,
                              height: 40.0,
                              child: CircleAvatar(
                                child: Icon(
                                  Icons.payment,
                                  size: 25.0,
                                  color: Colors.white,
                                ),
                                backgroundColor: Colors.blue[700].withOpacity(0.9),
                              ),
                            ),
                            SizedBox(width: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Pembayaran SPP Kuliah Berhasil", style: TextStyle(color: Colors.blue[700], fontSize: 14.0, fontWeight: FontWeight.bold)),
                            SizedBox(height: 10.0,),
                            Text("05 Mei 2020   13.10", style: TextStyle(color: Colors.blue[700], fontSize: 10.0)),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
  );
}

_donasikedua(){
  return Column(
    children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40.0,
                              height: 40.0,
                              child: CircleAvatar(
                                child: Icon(
                                  Icons.developer_board,
                                  size: 25.0,
                                  color: Colors.white,
                                ),
                                backgroundColor: Colors.yellow[700].withOpacity(0.9),
                              ),
                            ),
                            SizedBox(width: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Pembayaran Praktikum Tertunda", style: TextStyle(color: Colors.yellow[700], fontSize: 14.0, fontWeight: FontWeight.bold)),
                            SizedBox(height: 10.0,),
                            Text("05 Mei 2020   12:30", style: TextStyle(color: Colors.yellow[700], fontSize: 10.0)),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
  );
}

_donasiketiga(){
  return Column(
    children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40.0,
                              height: 40.0,
                              child: CircleAvatar(
                                child: Icon(
                                  Icons.account_balance_wallet,
                                  size: 25.0,
                                  color: Colors.white,
                                ),
                                backgroundColor: Colors.lightGreen[500].withOpacity(0.9),
                              ),
                            ),
                            SizedBox(width: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Donasi Palang Merah Indonesia", style: TextStyle(color: Colors.lightGreen[500], fontSize: 14.0, fontWeight: FontWeight.bold)),
                            SizedBox(height: 10.0,),
                            Text("05 - 13 Mei 2020", style: TextStyle(color: Colors.lightGreen[500], fontSize: 10.0)),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
  );
}

_donasikeempat(){
  return Column(
    children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40.0,
                              height: 40.0,
                              child: CircleAvatar(
                                child: Icon(
                                  Icons.payment,
                                  size: 25.0,
                                  color: Colors.white,
                                ),
                                backgroundColor: Colors.red.withOpacity(0.9),
                              ),
                            ),
                            SizedBox(width: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Pembayaran SPP Kuliah Semester 6", style: TextStyle(color: Colors.red, fontSize: 14.0, fontWeight: FontWeight.bold)),
                            SizedBox(height: 10.0,),
                            Text("4 Mei - 1 Juni 2020", style: TextStyle(color: Colors.red, fontSize: 10.0)),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
  );
}

_donasikelima(){
  return Column(
    children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40.0,
                              height: 40.0,
                              child: CircleAvatar(
                                child: Icon(
                                  Icons.developer_board,
                                  size: 25.0,
                                  color: Colors.white,
                                ),
                                backgroundColor: Colors.red.withOpacity(0.9),
                              ),
                            ),
                            SizedBox(width: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Pembayaran Praktikum Semester 6", style: TextStyle(color: Colors.red, fontSize: 14.0, fontWeight: FontWeight.bold)),
                            SizedBox(height: 10.0,),
                            Text("16 Mei - 10 Mei 2020", style: TextStyle(color: Colors.red, fontSize: 12.0)),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
  );
}

}