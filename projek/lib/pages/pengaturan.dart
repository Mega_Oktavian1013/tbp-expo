import 'package:flutter/material.dart';

import 'package:projek/pages/home.dart';
import 'package:projek/pages/profile.dart';
import 'package:projek/pages/tentang.dart';
import 'package:projek/pages/bantuan.dart';

class Pengaturan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[300],
          title: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color : Colors.white,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (context) => MyApp()),
                          );
                        },
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("Pengaturan", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold)),
                        ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        body : PengaturanPage(),
      ),
    );
  }
}

class PengaturanPage extends StatefulWidget {
  @override
  _PengaturanPage createState() => _PengaturanPage();
}

class _PengaturanPage extends State<PengaturanPage> {

  bool val = false;

  onValue(bool newVal){
    setState(() {
      val = newVal;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 5.0),
          ),
          ListTile(
            leading: Icon(Icons.supervised_user_circle),
            title: Text('Akun'),
            trailing: Container(
              child: FlatButton.icon(
                onPressed: (){
                  Navigator.push(
                    context, 
                    MaterialPageRoute(builder: (context) => Profile()),
                  );
                },
                icon: Icon(Icons.arrow_forward_ios),
                label: Text(''),
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.notifications),
            title: Text('Pemberitahuan'),
            trailing: Container(
              child: Column(
                children: <Widget>[
                  Switch(
                    value: val,
                    onChanged: (newVal){
                      onValue(newVal);
                    }),
                ],
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.help),
            title: Text('Bantuan'),
            trailing: Container(
              child: FlatButton.icon(
                onPressed: (){
                  Navigator.push(
                    context, 
                    MaterialPageRoute(builder: (context) => Bantuan()),
                  );
                },
                icon: Icon(Icons.arrow_forward_ios),
                label: Text(''),
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.info),
            title: Text('Tentang Aplikasi'),
            trailing: Container(
              child: FlatButton.icon(
                onPressed: (){
                  Navigator.push(
                    context, 
                    MaterialPageRoute(builder: (context) => Tentang()),
                  );
                },
                icon: Icon(Icons.arrow_forward_ios),
                label: Text(''),
              ),
            ),
          ),
        ],
      ),
    );
  }
}