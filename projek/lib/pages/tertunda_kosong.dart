import 'package:flutter/material.dart';

import 'package:projek/pages/home.dart';

class TertundaKosong extends StatelessWidget {
 @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[300],
          title: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color : Colors.white,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (context) => MyApp()),
                          );
                        },
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("Transaksi Tertunda", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold)),
                        ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        body : TertundaKosongPage(),
      ),
    );
  }
}

class TertundaKosongPage extends StatefulWidget {
  @override
  _TertundaKosongPage createState() => _TertundaKosongPage();
}

class _TertundaKosongPage extends State<TertundaKosongPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Text(
              'Tidak ada transaksi tertunda',
              style: TextStyle(color: Colors.grey[400], fontSize: 18.0, fontWeight: FontWeight.bold, fontFamily: 'Segoe_Print'),
            ),
        ),
    );
  }
}