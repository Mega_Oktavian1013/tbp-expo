import 'package:flutter/material.dart';

import 'package:projek/pages/home.dart';

class Praktikum extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[300],
          title: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color : Colors.white,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (context) => MyApp()),
                          );
                        },
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("Praktikum", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold)),
                        ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        body : PraktikumPage(),
      ),
    );
  }
}
class PraktikumPage extends StatefulWidget {
 static const routeName = 'konfirmasi';
 const PraktikumPage({
   Key key,
 }) : super(key: key);
 
 
 @override
 _PraktikumPage createState() => _PraktikumPage();
}
 
class _PraktikumPage extends State<PraktikumPage> {

 @override
 Widget build(BuildContext context) {
   return Scaffold(
    body: contentBody(context),
   );
 }
 
 Widget contentBody(BuildContext context) {
   return SafeArea(
     child: Padding(
       padding: const EdgeInsets.only(left: 8.0, right: 8.0),
       child: Stack(
         children: <Widget>[
           ListView.builder(
             itemCount: 1,
             itemBuilder: (context, index) {
               return Column(
                 children: <Widget>[
                   SizedBox(height: 8.0),
                   contentOperator(),
                   SizedBox(height: 8.0),
                   contentPelanggan(),
                   SizedBox(height: 8.0),
                   contentPelanggan2(),
                   SizedBox(height: 68.0),
                 ],
               );
             },
           ),
           Positioned(
             left: 8.0,
             right: 8.0,
             bottom: 8.0,
             child: Container(
               decoration: BoxDecoration(
                 borderRadius: BorderRadius.circular(8.0),
                 color: Colors.blueGrey,
               ),
               child: FlatButton(
                 child: Text(
                   'Bayar',
                   style: TextStyle(color: Colors.white),
                 ),
                 onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (context) => MyApp()),
                          );
                        },
               ),
             ),
           ),
         ],
       ),
     ),
   );
 }
 
 Widget contentPelanggan() {
   return Container(
     margin: EdgeInsets.only(left: 8.0, right: 8.0),
     decoration: BoxDecoration(
       boxShadow: [
         BoxShadow(
           color: Colors.grey[400],
           blurRadius: 5.0,
           spreadRadius: 0.0,
           offset: Offset(4.0, 8.0),
         )
       ],
       color: Colors.white,
       borderRadius: BorderRadius.circular(8.0),
     ),
     child: Row(
       crossAxisAlignment: CrossAxisAlignment.start,
       children: <Widget>[
         Padding(
           padding: const EdgeInsets.only(left: 8.0, top: 8.0),
           child: Icon(
             Icons.bubble_chart,
             color: Colors.blueGrey,
           ),
         ),
         Expanded(
           child: Padding(
             padding: const EdgeInsets.only(
               left: 8.0,
               right: 8.0,
               top: 8.0,
               bottom: 8.0,
             ),
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
                  SizedBox(height: 6.0),
                 Text(
                   'KETERANGAN',
                   style: TextStyle(
                     fontSize: 13,
                     fontWeight: FontWeight.bold,
                   ),
                 ),
                 SizedBox(height: 13.0),
                 Text(
                   'Semester',
                   style: TextStyle(
                     fontWeight: FontWeight.bold,
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 6.0),
                 Text(
                   '6',
                   style: TextStyle(
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 10.0),
                 Text(
                   'Jumlah Praktikum',
                   style: TextStyle(
                     fontSize: 12,
                     fontWeight: FontWeight.bold,
                   ),
                 ),
                 SizedBox(height: 6.0),
                 Text(
                   '4',
                   style: TextStyle(
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 10.0),
                 Text(
                   'Kekurangan Bayar',
                   style: TextStyle(
                     fontSize: 12,
                     fontWeight: FontWeight.bold,
                   ),
                 ),
                 SizedBox(height: 6.0),
                 Text(
                   'Rp 20.000,00',
                   style: TextStyle(
                     fontSize: 12,
                     color: Colors.red,
                   ),
                 ),
                 SizedBox(height: 10.0),
                 Text(
                   'Jumlah Terbayar',
                   style: TextStyle(
                     fontSize: 12,
                     fontWeight: FontWeight.bold,
                   ),
                 ),
                 SizedBox(height: 6.0),
                 Text(
                   'Rp 0',
                   style: TextStyle(
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 10.0),
                 Text(
                   'Kelebihan Bayaran',
                   style: TextStyle(
                     fontSize: 12,
                     fontWeight: FontWeight.bold,
                   ),
                 ),
                 SizedBox(height: 6.0),
                 Text(
                   'Rp 0',
                   style: TextStyle(
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 10.0),
               ],
             ),
           ),
         ),
       ],
     ),
   );
 }

 Widget contentPelanggan2() {
   return Container(
     margin: EdgeInsets.only(left: 8.0, right: 8.0),
     decoration: BoxDecoration(
       boxShadow: [
         BoxShadow(
           color: Colors.grey[400],
           blurRadius: 5.0,
           spreadRadius: 0.0,
           offset: Offset(4.0, 8.0),
         )
       ],
       color: Colors.white,
       borderRadius: BorderRadius.circular(8.0),
     ),
     child: Row(
       crossAxisAlignment: CrossAxisAlignment.start,
       children: <Widget>[
         Padding(
           padding: const EdgeInsets.only(left: 8.0, top: 8.0),
           child: Icon(
             Icons.bubble_chart,
             color: Colors.blueGrey,
           ),
         ),
         Expanded(
           child: Padding(
             padding: const EdgeInsets.only(
               left: 8.0,
               right: 8.0,
               top: 8.0,
               bottom: 8.0,
             ),
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
                  SizedBox(height: 6.0),
                 Text(
                   'TRANSAKSI PEMBAYARAN',
                   style: TextStyle(
                     fontSize: 13,
                     fontWeight: FontWeight.bold,
                   ),
                 ),
                 SizedBox(height: 13.0),
                 semester,
                 SizedBox(height: 6.0),
                 jumlah,
                  SizedBox(height: 6.0),
                  deskripsi,
                   SizedBox(height: 6.0),
               ],
             ),
           ),
         ),
       ],
     ),
   );
 }

 final semester = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      initialValue: '',
      decoration: InputDecoration(
        hintText: 'Semester',
        hintStyle: TextStyle(fontSize: 12),
      ),
  );

  final jumlah = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      initialValue: '',
      decoration: InputDecoration(
        hintText: 'Masukkan Nominal',
        hintStyle: TextStyle(fontSize: 12),
      ),
  );

  final deskripsi = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      initialValue: '',
      decoration: InputDecoration(
        hintText: 'Deskripsi',
        hintStyle: TextStyle(fontSize: 12),
      ),
  );

 final logo = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child : Center(
          child : CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 70.0,
            child: Image.asset('assets/mandiri.png'),
          ),
        ),
      ),
    );
 
 Widget contentOperator() {
   return Container(
     margin: EdgeInsets.only(left: 8.0, right: 8.0),
     decoration: BoxDecoration(
       boxShadow: [
         BoxShadow(
           color: Colors.grey[400],
           blurRadius: 5.0,
           spreadRadius: 0.0,
           offset: Offset(4.0, 8.0),
         )
       ],
       color: Colors.white,
       borderRadius: BorderRadius.circular(8.0),
     ),
     child: Row(
       crossAxisAlignment: CrossAxisAlignment.start,
       children: <Widget>[
         Padding(
           padding: const EdgeInsets.only(left: 8.0, top: 8.0),
           child: Icon(
             Icons.bubble_chart,
             color: Colors.blueGrey,
           ),
         ),
         Expanded(
           child: Padding(
             padding: const EdgeInsets.only(
               left: 8.0,
               right: 8.0,
               top: 8.0,
               bottom: 8.0,
             ),
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
                 logo,
                 Text(
                   'Mahasiswa',
                   style: TextStyle(
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 6.0),
                 Text(
                   'Mega Oktaviani Fadillah',
                   style: TextStyle(
                     fontWeight: FontWeight.bold,
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 10.0),
                 Text(
                   'Nomor Rekening',
                   style: TextStyle(
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 6.0),
                 Text(
                   '1881500095709',
                   style: TextStyle(
                     fontWeight: FontWeight.bold,
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 10.0),
                 Text(
                   'Saldo',
                   style: TextStyle(
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 6.0),
                 Text(
                   'Rp 20.0000.000',
                   style: TextStyle(
                     fontWeight: FontWeight.bold,
                     fontSize: 12,
                   ),
                 ),
                 SizedBox(height: 10.0),
               ],
             ),
           ),
         ),
       ],
     ),
   );
 }
}