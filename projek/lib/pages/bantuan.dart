import 'package:flutter/material.dart';

import 'package:projek/pages/pengaturan.dart';

class Bantuan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[300],
          title: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color : Colors.white,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (context) => Pengaturan()),
                          );
                        },
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("Bantuan", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold)),
                        ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        body : BantuanPage(),
      ),
    );
  }
}

class BantuanPage extends StatefulWidget {
  @override
  _BantuanPage createState() => _BantuanPage();
}

class _BantuanPage extends State<BantuanPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
      Container(
        padding: EdgeInsets.symmetric(vertical: 50.0),
        child: Center(
          child: Column(
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 100.0,
                child: Image.asset('assets/logo.jpeg'),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 100.0),
                child: Center(
                  child: Text(
                  "Hubungi @artecompany1991.gamil.com",
                  style: TextStyle(color: Colors.lightBlue[700], fontSize: 15.0, fontWeight: FontWeight.bold),
                ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}