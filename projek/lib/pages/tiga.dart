import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:projek/pages/donasi.dart';

class Tiga extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[300],
          title: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color : Colors.white,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (context) => Donasi()),
                          );
                        },
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("Donasi", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold)),
                        ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        body : TigaPage(),
      ),
    );
  }
}

class TigaPage extends StatefulWidget {
  @override
  _TigaPage createState() => _TigaPage();
}

class _TigaPage extends State<TigaPage> {
  _launchURL() async {
  const url = 'https://www.dompetdhuafa.org/id/Donasi';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

  Widget titleSection = Container(
      padding: EdgeInsets.only(top: 16),
      child: Text(
        'Dompet Dhuafa',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 16,
        ),
      ),
    ); 

    Widget subtitleSection = Container(
      padding: EdgeInsets.only(top: 16),
      child: Text(
        '02 Mei - 4 Juni 2020',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 14,
          color: Colors.red,
        ),
      ),
    );

    Widget descriptionSection = Container(
      padding: EdgeInsets.all(16),
      child: Text(
        'Dompet Dhuafa Republika lembaga Islam yang berkhidmat dalam pemberdayaan kaum '
        'dhuafa dengan pendekatan budaya melalui kegiatan filantropis dan wirausaha sosial profetik.',
        textAlign: TextAlign.justify,
        style: TextStyle(
          fontSize: 14,
        ),
      )
    );


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
          children: <Widget>[
            Image.asset('assets/donasi3.jpg'),
            Container(
              padding: EdgeInsets.only(bottom: 24),
            ),
            titleSection,
            subtitleSection,
            descriptionSection,
            Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            child: FlatButton(
                              onPressed: _launchURL,
                              color: Colors.lightBlue[300],
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: Text("Halaman", style: TextStyle(color: Colors.white, fontSize: 13.0)),
                            ),
                          ),
                        ],
                      ),
                    ),
          ]
      )
     );
  }
}