import 'package:flutter/material.dart';

import 'package:projek/pages/home.dart';
import 'package:projek/pages/satu.dart';
import 'package:projek/pages/dua.dart';
import 'package:projek/pages/tiga.dart';

class Donasi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[300],
          title: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color : Colors.white,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (context) => MyApp()),
                          );
                        },
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("SPP Kuliah", style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold)),
                        ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        body : DonasiPage(),
      ),
    );
  }
}

class DonasiPage extends StatefulWidget {
  @override
  _Donasi createState() => _Donasi();
}

class _Donasi extends State<DonasiPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          _donasipertama(),
          _donasikedua(),
          _donasiketiga(),
        ],
      ),
    );
  }
  _donasipertama(){
  return Column(
    children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40.0,
                              height: 40.0,
                              child: CircleAvatar(
                                backgroundColor: Colors.lightBlue[300],
                                foregroundColor: Colors.lightBlue[300],
                                backgroundImage: new AssetImage('assets/donasi2.png')
                              ),
                            ),
                            SizedBox(width: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Panti Asuhan Indonesia", style: TextStyle(color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.bold)),
                            SizedBox(height: 10.0,),
                            Text("05 - 21 Mei 2020", style: TextStyle(color: Colors.red, fontSize: 10.0)),
                          ],
                        )
                      ],
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            child: FlatButton(
                              onPressed: () {
                                Navigator.push(
                                  context, 
                                  MaterialPageRoute(builder: (context) => Satu()),
                                );
                              },
                              color: Colors.lightBlue[300],
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: Text("Rincian", style: TextStyle(color: Colors.white, fontSize: 13.0)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
  );
}

_donasikedua(){
  return Column(
    children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40.0,
                              height: 40.0,
                              child: CircleAvatar(
                                backgroundColor: Colors.lightBlue[300],
                                foregroundColor: Colors.lightBlue[300],
                                backgroundImage: new AssetImage('assets/donasi1.jpg')
                              ),
                            ),
                            SizedBox(width: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Palang Merah Indonesia", style: TextStyle(color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.bold)),
                            SizedBox(height: 10.0,),
                            Text("05 - 13 Mei 2020", style: TextStyle(color: Colors.red, fontSize: 10.0)),
                          ],
                        )
                      ],
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            child: FlatButton(
                              onPressed: () {
                                Navigator.push(
                                  context, 
                                  MaterialPageRoute(builder: (context) => Dua()),
                                );
                              },
                              color: Colors.lightBlue[300],
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: Text("Rincian", style: TextStyle(color: Colors.white, fontSize: 13.0)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
  );
}

_donasiketiga(){
  return Column(
    children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40.0,
                              height: 40.0,
                              child: CircleAvatar(
                                backgroundColor: Colors.lightBlue[300],
                                foregroundColor: Colors.lightBlue[300],
                                backgroundImage: new AssetImage('assets/donasi3.jpg')
                              ),
                            ),
                            SizedBox(width: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Dompet Dhuafa", style: TextStyle(color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.bold)),
                            SizedBox(height: 10.0,),
                            Text("02 Mei - 4 Juni 2020", style: TextStyle(color: Colors.red, fontSize: 10.0)),
                          ],
                        )
                      ],
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            child: FlatButton(
                              onPressed: () {
                                Navigator.push(
                                  context, 
                                  MaterialPageRoute(builder: (context) => Tiga()),
                                );
                              },
                              color: Colors.lightBlue[300],
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: Text("Rincian", style: TextStyle(color: Colors.white, fontSize: 13.0)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
  );
}

}